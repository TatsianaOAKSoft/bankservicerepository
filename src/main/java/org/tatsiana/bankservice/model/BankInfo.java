package org.tatsiana.bankservice.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class BankInfo implements Serializable {

    private long bankId;
    private String name;
    private String INN;
    private String correspondentAccount;
    private String BIK;
    private String address;
    private boolean isClosed;

    public BankInfo() {
    }

    public BankInfo(long bankId, String name, String INN, String correspondentAccount, String BIK, String address, boolean isClosed) {
        this.bankId = bankId;
        this.name = name;
        this.INN = INN;
        this.correspondentAccount = correspondentAccount;
        this.BIK = BIK;
        this.address = address;
        this.isClosed = isClosed;
    }

    public long getBankId() {
        return bankId;
    }

    public void setBankId(long bankId) {
        this.bankId = bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

    public String getCorrespondentAccount() {
        return correspondentAccount;
    }

    public void setCorrespondentAccount(String correspondentAccount) {
        this.correspondentAccount = correspondentAccount;
    }

    public String getBIK() {
        return BIK;
    }

    public void setBIK(String BIK) {
        this.BIK = BIK;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    @Override
    public String toString() {
        return "bankInfo" +
                "bankId=" + bankId +
                ", name='" + name + '\'' +
                ", INN='" + INN + '\'' +
                ", correspondentAccount='" + correspondentAccount + '\'' +
                ", BIK='" + BIK + '\'' +
                ", address='" + address + '\'' +
                ", isClosed=" + isClosed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankInfo bankInfo = (BankInfo) o;
        return bankId == bankInfo.bankId &&
                isClosed == bankInfo.isClosed &&
                Objects.equals(name, bankInfo.name) &&
                Objects.equals(INN, bankInfo.INN) &&
                Objects.equals(correspondentAccount, bankInfo.correspondentAccount) &&
                Objects.equals(BIK, bankInfo.BIK) &&
                Objects.equals(address, bankInfo.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bankId, name, INN, correspondentAccount, BIK, address, isClosed);
    }
}
