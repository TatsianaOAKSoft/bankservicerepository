package org.tatsiana.bankservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class ProviderBankInfo implements Serializable {

    @JsonIgnore
    private int agentId;
    private OperatorInfo providerInfo;
    private BankInfo bankInfo;

    public ProviderBankInfo() {
    }

    public ProviderBankInfo(int agentId, OperatorInfo providerInfo, BankInfo bankInfo) {
        this.agentId = agentId;
        this.providerInfo = providerInfo;
        this.bankInfo = bankInfo;
    }

    @JsonIgnore
    public int getAgentId() {
        return agentId;
    }

    @JsonProperty
    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    public OperatorInfo getProviderInfo() {
        return providerInfo;
    }

    public void setProviderInfo(OperatorInfo providerInfo) {
        this.providerInfo = providerInfo;
    }

    public BankInfo getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(BankInfo bankInfo) {
        this.bankInfo = bankInfo;
    }

    @Override
    public String toString() {
        return "ProviderBankInfo{" +
                "agentId=" + agentId +
                ", providerInfo=" + providerInfo +
                ", bankInfo=" + bankInfo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProviderBankInfo that = (ProviderBankInfo) o;
        return agentId == that.agentId &&
                Objects.equals(providerInfo, that.providerInfo) &&
                Objects.equals(bankInfo, that.bankInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentId, providerInfo, bankInfo);
    }
}
