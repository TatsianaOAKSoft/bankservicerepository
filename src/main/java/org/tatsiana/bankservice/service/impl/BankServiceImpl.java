package org.tatsiana.bankservice.service.impl;

import org.springframework.stereotype.Service;
import org.tatsiana.bankservice.model.ProviderBankInfo;
import org.tatsiana.bankservice.model.StaticData;
import org.tatsiana.bankservice.service.BankService;

import java.util.List;

@Service
public class BankServiceImpl implements BankService {

    public static List<ProviderBankInfo> providerBankInfoList = StaticData.getProviderBankInfoList();

    @Override
    public ProviderBankInfo getByAgentId(int agentId) {
        for (ProviderBankInfo providerBank : providerBankInfoList) {
            if (providerBank.getAgentId() == agentId) {
                return providerBank;
            }
        }
        return null;
    }

    @Override
    public ProviderBankInfo getByAgentIdAndBankId(String agentId, String bankId) {

        ProviderBankInfo providerBankByAgentId = null;

        if (!"".equals(agentId)) {
            int agentIdInt = Integer.valueOf(agentId);
            providerBankByAgentId = getByAgentId(agentIdInt);
            if (!"".equals(bankId) && providerBankByAgentId != null) {
                int bankIdInt = Integer.valueOf(bankId);
                if (providerBankByAgentId.getBankInfo().getBankId() == bankIdInt) {
                    return providerBankByAgentId;
                }
                return providerBankByAgentId;
            } else return providerBankByAgentId;
        } else {
            return null;
        }
    }
}
