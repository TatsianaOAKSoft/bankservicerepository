package org.tatsiana.bankservice.service;

import org.tatsiana.bankservice.model.ProviderBankInfo;

public interface BankService {

    ProviderBankInfo getByAgentId(int agentId);
    ProviderBankInfo getByAgentIdAndBankId(String agentId, String bankId);

}
