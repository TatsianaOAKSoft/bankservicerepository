package org.tatsiana.bankservice.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.tatsiana.bankservice.model.BankInfo;
import org.tatsiana.bankservice.model.OperatorInfo;
import org.tatsiana.bankservice.model.ProviderBankInfo;
import org.tatsiana.bankservice.service.BankService;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:web/WEB-INF/dispatcher-servlet.xml"
})
@WebAppConfiguration
public class MainControllerTest {

    private static final int AGENT_ID = 5;

    private static final String AGENT_ID_STR = "5";
    private static final String BANK_ID_STR = "5";

    private static final OperatorInfo providerInfo5 = new OperatorInfo(5, "pp", "pp", "pp");
    private static final BankInfo bankBaseInfo5 = new BankInfo(5, "p", "p", "p", "p", "p", true);
    private static final ProviderBankInfo providerBankInfo = new ProviderBankInfo(AGENT_ID, providerInfo5, bankBaseInfo5);

    @Autowired
    private BankService bankServiceMock;

    @Autowired
    private MainController mainController;

    private MockMvc mock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mock = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void testServiceFindByAgentId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentId(5);
        assertThat(providerBankInfo, is(agent));
    }

    @Test
    public void testServiceFindByNotExistAgentId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentId(10);
        assertThat(null, is(agent));
    }

    @Test
    public void testServiceFindByAgentIdAndBankId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentIdAndBankId(AGENT_ID_STR, BANK_ID_STR);
        assertThat(providerBankInfo, is(agent));
    }


    @Test
    public void testServiceFindByNotExistAgentIdAndBankId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentIdAndBankId("10", "1");
        assertEquals(null, agent);
    }

    @Test
    public void testServiceFindByNotExistAgentIdAndNotExistBankId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentIdAndBankId("10", "10");
        assertThat(null, is(agent));
    }

    @Test
    public void testServiceFindByAgentIdAndNotExistBankId() throws Exception {
        ProviderBankInfo agent = bankServiceMock.getByAgentIdAndBankId("5", "10");
        assertEquals(providerBankInfo, agent);
    }

    @Test
    public void testGetProviderBankInfo() throws Exception {
        mock.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(providerBankInfo, mainController.getProviderBankInfo(AGENT_ID_STR, BANK_ID_STR));
    }

    @Test
    public void testGetProviderBankInfoWithEmptyBankId() throws Exception {
        mock.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(providerBankInfo, mainController.getProviderBankInfo(AGENT_ID_STR, ""));
    }

    @Test
    public void testGetNULLProviderBankInfoWithEmptyAgentId() throws Exception {
        mock.perform(get("/agents/{agentId}/providerBankInfo", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(null, mainController.getProviderBankInfo("", ""));
    }

    @Test
    public void testPostProviderBankInfoByAgentIdAndBankId() throws Exception {
        mock.perform(MockMvcRequestBuilders.post("/agents/{agentId}/providerBankInfo/search", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(providerBankInfo, mainController.getAllProviderBankInfo(AGENT_ID_STR, BANK_ID_STR));
    }

    @Test
    public void testPostProviderBankInfoByAgentIdAndEmptyBankId() throws Exception {
        mock.perform(MockMvcRequestBuilders.post("/agents/{agentId}/providerBankInfo/search", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(providerBankInfo, mainController.getAllProviderBankInfo(AGENT_ID_STR, ""));
    }

    @Test
    public void testPostProviderBankInfoByAgentEmptyIdAndEmptyBankId() throws Exception {
        mock.perform(MockMvcRequestBuilders.post("/agents/{agentId}/providerBankInfo/search", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        assertEquals(null, mainController.getAllProviderBankInfo("", ""));
    }

    @Test
    public void testPostJSON() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        String str = mapper.writeValueAsString(providerBankInfo);

        JSONParser parser = new JSONParser();
        JSONObject object = (JSONObject) parser.parse(str);

        this.mock.perform(MockMvcRequestBuilders.post("/agents/{agentId}/providerBankInfo/search", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").value(object));
    }

    @Test
    public void testGetJSON() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        String str = mapper.writeValueAsString(providerBankInfo);

        JSONParser parser = new JSONParser();
        JSONObject object = (JSONObject) parser.parse(str);

        this.mock.perform(MockMvcRequestBuilders.get("/agents/{agentId}/providerBankInfo", AGENT_ID_STR)
                .param("bankId", BANK_ID_STR)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").value(object));
    }

    @Test
    public void testCountMethodesInvokes() throws Exception {
        BankService providerBankInfo2 = Mockito.mock(BankService.class);
        when(providerBankInfo2.getByAgentId(5)).thenReturn(providerBankInfo);

        providerBankInfo2.getByAgentId(5);
        providerBankInfo2.getByAgentId(6);

        verify(providerBankInfo2).getByAgentId(Matchers.eq(5));

        verify(providerBankInfo2, times(1)).getByAgentId(5);
        verify(providerBankInfo2, times(1)).getByAgentId(6);

        //called never
        verify(providerBankInfo2, never()).getByAgentIdAndBankId(AGENT_ID_STR, BANK_ID_STR);
        //called at least one
        verify(providerBankInfo2, atLeastOnce()).getByAgentId(5);
        //"called at least twice"
        verify(providerBankInfo2, atLeast(1)).getByAgentId(5);
        //called at most 3 times
        verify(providerBankInfo2, atMost(3)).getByAgentId(5);
    }

}
